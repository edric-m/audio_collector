fileIn = open("mp3s/untitled.mp3", 'rb') # read mp3 as a binary file

print("\n***\nImported file: " + fileIn.name + "\nRead as a binary file\n***\n")
linesReadCtr = 0

print("Read line = 'r' \nQuit = 'q'")
userInput = input()
while userInput != 'q':
    if userInput == 'r':
        linesReadCtr = linesReadCtr + 1
        print("\n" + "Line:", linesReadCtr)
        print(fileIn.readline())
    print("\nRead line = 'r' \nQuit = 'q'")
    userInput = input()

fileIn.close()
